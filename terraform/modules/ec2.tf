resource "aws_instance" "Instance" {
    ami = var.ami_id
    instance_type = var.instance_type
    associate_public_ip_address = var.public_or_private
    key_name = var.key_name

    tags = {
      Name = var.instance_name
  }  
}