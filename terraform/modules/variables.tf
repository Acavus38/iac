variable "instance_type" {
  description = "The type of instance to start."
  default     = "t2.micro"
}

variable "ami_id" {}
 
variable "instance_name"{}

variable "public_or_private" {}

variable "key_name" {}