module "public_instance"{
  source = "./modules"
  ami_id = "ami-0006ba1ba3732dd33"
  instance_type = "t2.micro"
  public_or_private = "true"
  key_name = "iac"

  instance_name = "Public Instance"

}